package com.array;

import java.util.Scanner;

public class MaximumMinimumDifference {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfElements = scanner.nextInt();
        int[] arrayElements = new int[numberOfElements];
        for (int i = 0; i < numberOfElements; i++){
            arrayElements[i] = scanner.nextInt();
        }
        System.out.println(" Difference between Maximum and Minimum Element " + Difference(arrayElements));
    }

    private static int Difference(int[] arrayElements) {
        int minimumElement = arrayElements[0];
        int maximumElement = arrayElements[0];
        for(int i=1; i<arrayElements.length; i++){
            if( minimumElement > arrayElements[i] ) minimumElement = arrayElements[i];
            if( maximumElement < arrayElements[i] ) maximumElement = arrayElements[i];
        }
        return maximumElement - minimumElement ;
    }
}

 /*   Input
        5
        1
        22
        100
        34
        55

      Output
        99
  */
